---
title: V19 Configuration
metaTitle: V19 Configuration | Psono Documentation
meta:
  - name: description
    content: Configuration TBD
---

# V19 Configuration

Configuration TBD

[[toc]]

| ID   | Detailed Verification Requirement                                                                                                                                                                                                                                   | Level 1                 | Level 2    | Level 3     | Since     |
|-------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|---|-------|
| 19.1  | All components should be up to date with proper security configuration(s) and version(s). This should include removal of unneeded configurations and folders such as sample applications, platform documentation, and default or example users. | x | x | x | 3.0   |
| 19.2  | Communications between components, such as between the application server and the database server, should be encrypted, particularly when the components are in different containers or on different systems.                                   |   | x | x | 3.0   |
| 19.3  | Communications between components, such as between the application server and the database server should be authenticated using an account with the least necessary privileges.                                                                 |   | x | x | 3.0   |
| 19.4  | Verify application deployments are adequately sandboxed, containerized or isolated to delay and deter attackers from attacking other applications.                                                                                              |   | x | x | 3.0   |
| 19.5  | Verify that the application build and deployment processes are performed in a secure fashion.                                                                                                                                                   |   | x | x | 3.0   |
| 19.6  | Verify that authorised administrators have the capability to verify the integrity of all security-relevant configurations to ensure that they have not been tampered with.                                                                      |   |   | x | 3.0   |
| 19.7  | Verify that all application components are signed.                                                                                                                                                                                              |   |   | x | 3.0   |
| 19.8  | Verify that third party components come from trusted repositories.                                                                                                                                                                              |   |   | x | 3.0   |
| 19.9  | Ensure that build processes for system level languages have all security flags enabled, such as ASLR, DEP, and security checks.                                                                                                                 |   |   | x | 3.0   |
| 19.10 | Verify that all application assets are hosted by the application, such as JavaScript libraries, CSS stylesheets and web fonts are hosted by the application rather than rely on a CDN or external provider.                                     |   |   | x | 3.0.1 |

### 19.1

All components are kept up to date. There are multiple scanners and checks in place to check for security vulnerabilities.

### 19.2

All traffic between components is encrypted. It's in general the administrators responsibility to use e.g. encryption for the Database connection. Psono.pw is configured correctly.

### 19.3

Application wise there is no requirement that would prevent this. In general it is the administrators to use a user with the least necessary privileges. Psono.pw is configured correctly.

### 19.4

Application deployments are completely sandboxed with Gitlab Runner.

### 19.5

Build and deployment process is performed in an secure environment with manualy approval.

### 19.6 (violation)

Containers are currently not signed.

### 19.7 (violation)

Containers are currently not signed.

### 19.8

Components come from trusted sources as far as it is possible in the open source field with public package repositories.

### 19.9

Python enabled ASLR and DEP since Python 3.4

### 19.10

All assets are hosed by the application and not on a CDN or external provider.
