---
title: V18 Web Services
metaTitle: V18 Web Services | Psono Documentation
meta:
  - name: description
    content: Web services verification requirements
---

# V18 Web Services

Web services verification requirements

[[toc]]


| ID   | Detailed Verification Requirement                                                                                                                                                                         | Level 1                 | Level 2    | Level 3     | Since     |
|-------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|---|-------|
| 18.1  | Verify that the same encoding style is used between the client and the server.                                                                                                                                 | x | x | x | 3.0   |
| 18.2  | Verify that access to administration and management functions within the Web Service Application is limited to web service administrators.                                                                     | x | x | x | 3.0   |
| 18.3  | Verify that XML or JSON schema is in place and verified before accepting input.                                                                                                                                | x | x | x | 3.0   |
| 18.4  | Verify that all input is limited to an appropriate size limit.                                                                                                                                                 | x | x | x | 3.0   |
| 18.5  | Verify that SOAP based web services are compliant with Web Services-Interoperability (WS-I) Basic Profile at minimum. This essentially means TLS encryption.                                                   | x | x | x | 3.0.1 |
| 18.6  | Verify the use of session-based authentication and authorization. Please refer to sections 2, 3 and 4 for further guidance. Avoid the use of static "API keys" and similar.                                    | x | x | x | 3.0   |
| 18.7  | Verify that the REST service is protected from Cross-Site Request Forgery via the use of at least one or more of the following: ORIGIN checks, double submit cookie pattern, CSRF nonces, and referrer checks. | x | x | x | 3.0.1 |
| 18.8  | Verify the REST service explicitly check the incoming Content-Type to be the expected one, such as application/xml or application/json.                                                                        |   | x | x | 3.0   |
| 18.9  | Verify that the message payload is signed to ensure reliable transport between client and service, using JSON Web Signing or WS-Security for SOAP requests.                                                    |   | x | x | 3.0.1 |
| 18.10 | Verify that alternative and less secure access paths do not exist.                                                                                                                                             |   | x | x | 3.0   |


### 18.1

Both client and server use `utf-8`.

### 18.2

Access to API endpoints for management are all protected by authentication. Further Psono allows to turn them off completely which allows for example to create a special server for administration which administrators use.

### 18.3

JSON is in place and is validated by Django.

### 18.4 (violation)

Not all inputs are currently limited on the "validation" level.

### 18.5

Psono uses no SOAP.

### 18.6

Session based authentication and authorization is in place. Static API keys are not used for the normal user authentication. Psono offers static API keys for Applications to access Psono.

### 18.7

By default Psono is configured to prevent CSRF. The header based authentication of all API endpoints makes it in addition extremely difficult to craft a CSRF attack.

### 18.8

Psono checks for the `application/json` content type

### 18.9

The request payload is encrypted with NaCl encryption.

### 18.10

As far as on the application level possible, Psono is not exposing less secure access paths. As Psono is exposed with docker, it is the administrators responsibility to make sure to shut down all access to the docker ports.
