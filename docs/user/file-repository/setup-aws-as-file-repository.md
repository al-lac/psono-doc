---
title: Setup AWS as file repository
metaTitle: Setup AWS S3 as file repository | Psono Documentation
meta:
  - name: description
    content: Instructions how to setup an AWS S3 bucket as file repository
---

# Setup AWS S3 as file repository

Instructions how to setup an AWS S3 bucket as file repository



## Pre-requirements

You need to have an AWS account. If not, you can register here [aws.amazon.com](https://aws.amazon.com/)
As a new customer AWS will provide you with a lot of benefits, including 5 GB of free cloud storage in the first
12 months that you can use together with Psono. More details can be found here: [aws.amazon.com/free/](https://aws.amazon.com/free/)


## Setup Guide

This guide will walk you through the creation of a bucket, the configuration of the bucket and the creation of a
service account, before it helps you to configure it in psono.

### Create bucket

1) Login to aws.amazon.com

2) Go to S3

![Step 3 Go to s3](/images/user/file_repository_setup_aws/01-go-to-s3.jpg)

3) Click "Create bucket"

![Step 3 Create bucket](/images/user/file_repository_setup_aws/02-click-create-bucket.jpg)

4) Specify bucket information and click "Create"

![Step 3 Specify bucket information](/images/user/file_repository_setup_aws/03-specify-bucket-information.jpg)

::: tip
Remember the bucket name. You will need it later.
:::

### Configure CORS

1) Click on your bucket

![Step 1 Click on your bucket](/images/user/file_repository_setup_aws/11-click-your-bucket.jpg)

2) Go to "Permissions" > "CORS configuration"

![Step 2 Go to Permissions - CORS configuration](/images/user/file_repository_setup_aws/12-click-cors-configuration.jpg)

and paste the following config:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
<CORSRule>
    <AllowedOrigin>*</AllowedOrigin>
    <AllowedMethod>GET</AllowedMethod>
    <AllowedMethod>PUT</AllowedMethod>
    <AllowedMethod>POST</AllowedMethod>
    <AllowedMethod>DELETE</AllowedMethod>
    <AllowedHeader>*</AllowedHeader>
</CORSRule>
</CORSConfiguration>
```

3) Click "Save"


### Create a policy

1) Go to IAM

![Step 1 Go to IAM](/images/user/file_repository_setup_aws/31-go-to-iam.jpg)

2) Go to Policies and click "Create Policy"

![Step 2 Go to Policies and click Create Policy](/images/user/file_repository_setup_aws/21-go-to-pilicies.jpg)

3) Select JSON

![Step 3 Go to JSON](/images/user/file_repository_setup_aws/22-select-json-and-paste-content.jpg)

and paste the following config:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ListObjectsInBucket",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::psono-file-uploads"
            ]
        },
        {
            "Sid": "AllObjectActions",
            "Effect": "Allow",
            "Action": "s3:*Object",
            "Resource": [
                "arn:aws:s3:::psono-file-uploads/*"
            ]
        }
    ]
}
```


::: tip
Replace psono-file-uploads with your bucket name
:::

4) Click "Review Policy"

5) Specify a name and description

![Step 5 Specify a name and description](/images/user/file_repository_setup_aws/23-specify-name-and-description.jpg)

6) Click "Create Policy"


### Create an user

1) Go to IAM

![Step 1 Go to IAM](/images/user/file_repository_setup_aws/31-go-to-iam.jpg)

2) Go to users and click "Add User"

![Step 2 Go to users and click add user](/images/user/file_repository_setup_aws/32-click-add-user.jpg)

3) Specify a "name" and allow "programmatic access"

![Step 3 Specify some user information](/images/user/file_repository_setup_aws/33-specify-user-infos.jpg)

4) Attach your policy

![Step 4 Attach your policy](/images/user/file_repository_setup_aws/34-attach-policy.jpg)

5) Acquire "Access key ID" and "Secret access key"

![Step 5 Acquire "Access key ID" and "Secret access key"](/images/user/file_repository_setup_aws/35-get-secret-access-key.jpg)



### Configure the file repository


1) Login to Psono

![Step 13 Login to Psono](/images/user/file_repository_setup_gcs/step13-login-to-psono.jpg)

2) Go to "Other"

![Step 14 Go to other](/images/user/file_repository_setup_gcs/step14-go-to-other.jpg)

3) Go to "File Repositories" and click "Create new file repository"

![Step 15 Go to "File Repositories" and click "Create new file repository"](/images/user/file_repository_setup_gcs/step15-select-file-repository-and-click-create-new-file-repository.jpg)

4) Configure the file repository

Use any descriptive title, select AWS S3 as type, add your bucket's name, access key id and secret access key.
As region you should specify a region that is closest to you from this [list](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.RegionsAndAvailabilityZones.html)

![Step 16 Configure the file repository](/images/user/file_repository_setup_aws/41-create-file-repository.jpg)

You can now upload files from the datastore to this file repository.





